from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import create_form
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = create_form(request.POST)
        if form.is_valid():
            task = form.save()
            task.owner = request.user
            form.save()
            return redirect("list_projects")
    else:
        form = create_form()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/list.html", context)
