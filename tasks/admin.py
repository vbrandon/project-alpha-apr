from django.contrib import admin
from tasks.models import Task


@admin.register(Task)
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )
