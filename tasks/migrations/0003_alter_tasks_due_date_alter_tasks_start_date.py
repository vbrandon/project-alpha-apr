# Generated by Django 4.1.5 on 2023-01-24 01:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("tasks", "0002_alter_tasks_due_date_alter_tasks_name"),
    ]

    operations = [
        migrations.AlterField(
            model_name="tasks",
            name="due_date",
            field=models.DateTimeField(auto_created=True),
        ),
        migrations.AlterField(
            model_name="tasks",
            name="start_date",
            field=models.DateTimeField(auto_created=True),
        ),
    ]
